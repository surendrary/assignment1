from flask import Flask,request,jsonify,json
from flask_sqlalchemy import SQLAlchemy
from Database import *

@app.route('/v1/expenses',methods=['POST'])
def  Authenticate():

	name = request.data
	dataDict = json.loads(name)
	username=dataDict.get('name')
	email=dataDict.get('email')
	category=dataDict.get('category')
	description=dataDict.get('description')
	link=dataDict.get('link')
	estimated_costs=dataDict.get('estimated_costs')
	submit_date=dataDict.get('submit_date')
	status="Pending"
	db.create_all()


	new_row=Expense_tb(username,email,category,description,link,estimated_costs,submit_date,"Pending","")
	db.session.add(new_row)
	db.session.commit()
	Query=Expense_tb.query.filter_by(username=username,email=email).first()
	vid=Query.id
	decision_date=Query.decision_date

	
	content ={'id':vid,'name': username,'email': email,'category': category,'description':description,'link': link,'estimated_costs':estimated_costs,'submit_date':submit_date,'status':status,'decision_date':decision_date}
		
	return jsonify(content),201
	

@app.route('/v1/expenses/<expense_id>',methods=['GET'])
def  getExpense(expense_id):
		
		Query=Expense_tb.query.filter_by(id=expense_id).first()
		if Query is None:
			return ("Content Not Found"),404

		else:
			name=Query.username
			email=Query.email
			category=Query.category
			description=Query.description
			link=Query.link
			estimated_cost=Query.estimated_cost
			submitted_date=Query.submitted_date
			status=Query.status
			decision_date=Query.decision_date

			content ={'id':expense_id,'name': name,'email': email,'category': category,'description':description,'link': link,'estimated_costs':estimated_cost,'submit_date':submitted_date,'status':status,'decision_date':decision_date}
			return jsonify(content),200
	

@app.route('/v1/expenses/<expense_id>',methods=['PUT'])
def Update(expense_id):

	name = request.data
	dataDict = json.loads(name)
	updated_cost= dataDict.get('estimated_costs')

	update_data=Expense_tb.query.filter_by(id=expense_id).first()
	if update_data is None:
		return("Record Not Found")
	else:
		update_data.estimated_cost=updated_cost
		db.session.commit()
		return jsonify("Successfully Updated"),202
	

@app.route('/v1/expenses/<expense_id>',methods=['DELETE'])
def Delete(expense_id):

	Delete_data=Expense_tb.query.filter_by(id=expense_id).first()
	if Delete_data is None:
		return jsonify("Connot be deleted"),204
	else:
		db.session.delete(Delete_data)
		db.session.commit()
		return jsonify("Successfully Deleted"),204


if __name__ == "__main__":
    app.run(host="0.0.0.0",port=5000,debug="true")
