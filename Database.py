from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine

DATABASE = 'ExpenseDb'
PASSWORD = 'surendra'
USER = 'root'
HOSTNAME = 'localhost'

app=Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://%s:%s@%s/%s'%(USER, PASSWORD, HOSTNAME, DATABASE)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
db = SQLAlchemy(app)
db.init_app(app)
engine=create_engine('mysql://root:surendra@localhost')
engine.execute("CREATE DATABASE IF NOT EXISTS %s "%(DATABASE))
db.create_all();


class Expense_tb(db.Model):
	__tablename__='expense_tb'
	id=db.Column(db.Integer,autoincrement=1,primary_key=True)
	username=db.Column(db.String(120))
	email=db.Column(db.String(50))
	category=db.Column(db.String(100))
	description=db.Column(db.String(100))
	link=db.Column(db.String(50))
	estimated_cost=db.Column(db.String(80))
	submitted_date=db.Column(db.String(40))
	status=db.Column(db.String(80))
	decision_date=db.Column(db.String(40))

	def __init__(self,username,email,category,description,link,estimated_cost,submitted_date,status,decision_date):#self,username,email,category,description,link,estimated_cost,submitted_date,status,decision_date): #u sername,email,category,description,link,estimated_cost,submitted_date,status):
	    self.username = username
	    self.email =email
	    self.category=category
	    self.description=description
	    self.link=link
	    self.estimated_cost=estimated_cost
	    self.submitted_date=submitted_date
	    self.status=status

