FROM python:latest
MAINTAINER Surendra Yadav "surendraryadav@gmail.com"
COPY . /app
WORKDIR /app
RUN apt-get update
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["Application.py"]
EXPOSE 5000